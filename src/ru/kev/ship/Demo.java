package ru.kev.ship;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс Demo , здесь производится запуск игры.
 *
 * В нём реализованны методы :
 * Размещение корабля на поле случайным образом;
 * Подсчёт кол-ва выстрелов за игру;
 * Стрельба по заданным координатам.
 *
 */

public class Demo {
    public static void main(String[] args) {

        ArrayList<String>localofShip = new ArrayList<>();
        Ship ship = new Ship();
        randomlocal(localofShip);
        ship.setShips(localofShip);
        System.out.println(ship);
        int countOfTry = 0 ;
        while (!ship.getLocation().isEmpty()) {
            System.out.println(ship.check(input()));
            countOfTry++;
        }
        System.out.println("Вы выйграли! Кол-во попыток: " +countOfTry);
    }

    /**
     * Метод , который случайно ставит корабль на поле.
     *
     * @param arrayList - содержит местоположение корабля.
     */
    public static void randomlocal(ArrayList<String> arrayList) {
        arrayList.add(String.valueOf((int) (Math.random()*7)));
        arrayList.add(String.valueOf(Integer.parseInt(arrayList.get(0))+1));
        arrayList.add(String.valueOf(Integer.parseInt(arrayList.get(1))+1));
    }

    /**
     * Метод , с помощью которого вводится координата выстрела по кораблю.
     *
     * @return - резуьтат.
     */
    public static String input() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите координаты выстрела от 0 до 9 : ");
        return String.valueOf(scanner.nextInt());
    }

}