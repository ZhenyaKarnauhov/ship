package ru.kev.ship;

import java.util.ArrayList;

/**
 * Класс Ship , в нём реализованны метод проверки координат корабля при выстреле.
 * И вывод результата после сделанного выстрела.
 */

public class Ship {
    private ArrayList<String> location;

    public void setShips(ArrayList<String> location) {
        this.location = location;
    }

    /**
     * Метод , выводит результат выстрела : "Ранен" & "Потоплен" & "Мимо".
     *
     * @param shot - выстрел.
     * @return - результат.
     */

    public String check(String shot) {
        String result="Мимо";
        if (location.indexOf(shot) != -1) {
            location.remove(location.indexOf(shot));
            result = "Ранен";
            if (location.isEmpty()) {
                result = "Потоплен";
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return "Ship" +
                " Location = " + location;
    }

    public ArrayList<String> getLocation() {
        return location;
    }
}
